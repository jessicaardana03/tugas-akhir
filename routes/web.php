<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DatakendaraanController;
use App\Http\Controllers\DatajadwalController;
use App\Http\Controllers\DatasopirController;
use App\Http\Controllers\DatatamanController;
use App\Http\Controllers\DatadepotController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/', [DatakendaraanController::class, 'index']);
    /* data kendaraan */
    Route::get('createdata', [DatakendaraanController::class, 'create'])->name('datakendaraan.create');
    Route::post('storedatakendaraan', [DatakendaraanController::class, 'store'])->name('datakendaraan.store');
    Route::get('/datakendaraan', [DatakendaraanController::class, 'index'])->name('datakendaraan.index');
    Route::get('/datakendaraan/{id}/edit', [DatakendaraanController::class, 'edit'])->name('datakendaraan.edit');
    Route::put('updatedatakendaraan/{id}', [DatakendaraanController::class, 'update'])->name('datakendaraan.update');
    Route::get('deletedatakendaraan/{id}', [DatakendaraanController::class, 'delete'])->name('datakendaraan.delete');

    /* jadwal */
    Route::get('createdatajadwal', [DatajadwalController::class, 'create'])->name('jadwal.create');
    Route::get('/datajadwal', [DatajadwalController::class, 'index'])->name('jadwal.index');
    Route::post('storedatajadwal', [DatajadwalController::class, 'store'])->name('jadwal.store');
    Route::get('/datajadwal/{id}/edit', [DatajadwalController::class, 'edit'])->name('jadwal.edit');
    Route::put('updatedatajadwal/{id}', [DatajadwalController::class, 'update'])->name('jadwal.update');
    Route::get('deletedatajadwal/{id}', [DatajadwalController::class, 'delete'])->name('jadwal.delete');


    /* data sopir */
    Route::get('createdatasopir', [DatasopirController::class, 'create'])->name('datasopir.create');
    Route::get('/datasopir', [DatasopirController::class, 'index'])->name('datasopir.index');
    Route::post('storedatasopir', [DatasopirController::class, 'store'])->name('datasopir.store');
    Route::get('/datasopir/{id}/edit', [DatasopirController::class, 'edit'])->name('datasopir.edit');
    Route::put('updatedatasopir/{id}', [DatasopirController::class, 'update'])->name('datasopir.update');
    Route::get('deletedatasopir/{id}', [DatasopirController::class, 'delete'])->name('datasopir.delete');


    /* data taman */
    Route::get('/datataman',  [DatatamanController::class, 'index'])->name('datataman.index');
    Route::get('createdatataman', [DatatamanController::class, 'create'])->name('datataman.create');
    Route::post('storedatataman', [DatatamanController::class, 'store'])->name('datataman.store');
    Route::get('/datataman/{id}/edit', [DatatamanController::class, 'edit'])->name('datataman.edit');
    Route::put('updatedatataman/{id}', [DatatamanController::class, 'update'])->name('datataman.update');
    Route::get('deletedata/{id}', [DatatamanController::class, 'delete'])->name('datataman.delete');

    Route::get('/datadepot', [DatadepotController::class, 'index'])->name('datadepot.index');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
