<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_tamen', function (Blueprint $table) {
            $table->id('id_taman');
            $table->string('nama_taman');
            $table->string('ukuran');
            $table->string('lokasi');
            $table->string('depot_air');
            $table->string('rute');
            $table->string('rayon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_tamen');
    }
}
