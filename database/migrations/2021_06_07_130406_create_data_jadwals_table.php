<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_jadwals', function (Blueprint $table) {
            $table->id('id_jadwal');
            $table->string('hari');
            $table->date('tanggal');
            $table->time('jam_mulai');
            $table->string('estimasi_air');
            $table->time('estimasi_selesai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_jadwals');
    }
}
