<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_kendaraans', function (Blueprint $table) {
            $table->id('id_kendaraan');
            $table->string('nama_sopir');
            $table->string('kapasitas_kendaraan');
            $table->string('kapasitas_air');
            $table->string('plat_kendaraan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_kendaraans');
    }
}
