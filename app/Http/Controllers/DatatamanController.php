<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\data_taman;

class DatatamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_taman=Data_Taman::get();
       return view('pages.datataman.index', compact('data_taman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('pages.datataman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama_taman = $request->nama_taman;
        $ukuran = $request->ukuran;
        $lokasi = $request->lokasi;
        $depot_air = $request->depot_air;
        $rute = $request->rute;
        $rayon = $request->rayon;
        $data_taman = new Data_Taman;
        $data_taman->nama_taman=$nama_taman;
        $data_taman->ukuran=$ukuran;
        $data_taman->lokasi=$lokasi;
        $data_taman->depot_air=$depot_air;
        $data_taman->rute=$rute;
        $data_taman->rayon=$rayon;
        $data_taman->save();
        return redirect()->route('datataman.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $datataman= data_taman::findorfail($id);
        return view('pages.datataman.edit', compact('datataman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama_taman= $request->nama_taman;
        $ukuran= $request->ukuran;
        $lokasi= $request->lokasi;
        $depot_air= $request->depot_air;
        $rute= $request->rute;
        $rayon= $request->rayon;
        $datataman = data_taman::find($id);
        $datataman->nama_taman= $nama_taman;
        $datataman->ukuran= $ukuran;
        $datataman->lokasi= $lokasi;
        $datataman->depot_air= $depot_air;
        $datataman->rute= $rute;
        $datataman->rayon= $rayon;
        $datataman->save();
        return redirect()->route('datataman.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $datataman=data_taman::findorfail($id);
        $datataman->delete();
        return back();
    }
}
