<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\data_kendaraan;

class DatakendaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datakendaraan= data_kendaraan::all();
        return view('pages.datakendaraan.index', compact('datakendaraan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.datakendaraan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama_sopir= $request->nama_sopir;
        $kapasitas_kendaraan= $request->kapasitas_kendaraan;
        $kapasitas_air= $request->kapasitas_air;
        $plat_kendaraan= $request->plat_kendaraan;
        $datakendaraan = new data_kendaraan;
        $datakendaraan->nama_sopir= $nama_sopir;
        $datakendaraan->kapasitas_kendaraan= $kapasitas_kendaraan;
        $datakendaraan->kapasitas_air= $kapasitas_air;
        $datakendaraan->plat_kendaraan= $plat_kendaraan;
        $datakendaraan->save();
        return redirect()->route('datakendaraan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datakendaraan= data_kendaraan::find($id);
        return view('pages.datakendaraan.edit', compact('datakendaraan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama_sopir= $request->nama_sopir;
        $kapasitas_kendaraan= $request->kapasitas_kendaraan;
        $kapasitas_air= $request->kapasitas_air;
        $plat_kendaraan= $request->plat_kendaraan;
        $datakendaraan = data_kendaraan::find($id);
        $datakendaraan->nama_sopir= $nama_sopir;
        $datakendaraan->kapasitas_kendaraan= $kapasitas_kendaraan;
        $datakendaraan->kapasitas_air= $kapasitas_air;
        $datakendaraan->plat_kendaraan= $plat_kendaraan;
        $datakendaraan->save();
        return redirect()->route('datakendaraan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $datakendaraan = data_kendaraan::findorfail($id);
        $datakendaraan->delete();
        return redirect()->route('datakendaraan.index');
    }
}
