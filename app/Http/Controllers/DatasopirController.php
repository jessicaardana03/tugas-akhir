<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\data_sopir;

class DatasopirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $datasopir= data_sopir::all();
        return view('pages.datasopir.index', compact('datasopir'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.datasopir.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama_sopir= $request->nama_sopir;
        $username= $request->username;
        $password= $request->password;
        $rayon= $request->rayon;
        $datasopir= new data_sopir;
        $datasopir->nama_sopir= $nama_sopir;
        $datasopir->username= $username;
        $datasopir->password= $password;
        $datasopir->rayon= $rayon;
        $datasopir->save();
        return redirect()->route('datasopir.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datasopir= data_sopir::find($id);
        return view('pages.datasopir.edit', compact('datasopir'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama_sopir= $request->nama_sopir;
        $username= $request->username;
        $password= $request->password;
        $rayon= $request->rayon;
        $datasopir= data_sopir::find($id);
        $datasopir->nama_sopir= $nama_sopir;
        $datasopir->username= $username;
        $datasopir->password= $password;
        $datasopir->rayon= $rayon;
        $datasopir->save();
        return redirect()->route('datasopir.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $datasopir = data_sopir::findorfail($id);
        $datasopir->delete();
        return redirect()->route('datasopir.index');
    }
}
