<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\data_jadwal;

class DatajadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_jadwals= Data_Jadwal::all();
        return view('pages.jadwal.index', compact('data_jadwals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.jadwal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hari= $request->hari;
        $tanggal= $request->tanggal;
        $jam_mulai= $request->jam_mulai;
        $estimasi_air= $request->estimasi_air;
        $estimasi_selesai= $request->estimasi_selesai;
        $data_jadwals = new Data_Jadwal;
        $data_jadwals->hari= $hari;
        $data_jadwals->tanggal= $tanggal;
        $data_jadwals->jam_mulai= $jam_mulai;
        $data_jadwals->estimasi_air= $estimasi_air;
        $data_jadwals->estimasi_selesai= $estimasi_selesai;
        $data_jadwals->save();
        return redirect()->route('jadwal.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_jadwals= data_jadwal::find($id);
        return view('pages.jadwal.edit', compact('data_jadwals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hari= $request->hari;
        $tanggal= $request->tanggal;
        $jam_mulai= $request->jam_mulai;
        $estimasi_air= $request->estimasi_air;
        $estimasi_selesai= $request->estimasi_selesai;
        $data_jadwals = Data_Jadwal::find($id);
        $data_jadwals->hari= $hari;
        $data_jadwals->tanggal= $tanggal;
        $data_jadwals->jam_mulai= $jam_mulai;
        $data_jadwals->estimasi_air= $estimasi_air;
        $data_jadwals->estimasi_selesai= $estimasi_selesai;
        $data_jadwals->save();
        return redirect()->route('jadwal.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_jadwals=Data_Jadwal::findorfail($id);
        $data_jadwals->delete();
        return back();
    }
}
