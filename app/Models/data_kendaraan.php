<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class data_kendaraan extends Model
{
    use HasFactory;
    protected $fillable=[
    	'nama_sopir','kapasitas_kendaraan','kapasitas_air','plat_kendaraan',
    ];
    protected $table ='data_kendaraans';
    protected $primaryKey='id_kendaraan';
}
