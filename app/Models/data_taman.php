<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class data_taman extends Model
{
    use HasFactory;
    protected $fillable=[
    	'nama_taman','ukuran','lokasi','depot_air','rute','rayon',
    ];
    protected $table ='data_tamen';
    protected $primaryKey='id_taman';
}

