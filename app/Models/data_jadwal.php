<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class data_jadwal extends Model
{
    use HasFactory;
    protected $fillable=[
    	'hari','tanggal','jam_mulai','estimasi_air','estimasi_selesai',
    ];
    protected $table ='data_jadwals';
    protected $primaryKey='id_jadwal';

    }
