@extends('welcome')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Tambah Jadwal Penyiraman</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambahkan Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  action="{{route ('jadwal.store')}}" method="POST"> 
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputHari">Hari</label>
                    <input type="text" name="hari" class="form-control" id="exampleInputhari" placeholder="Masukkan Hari Penyiraman">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputTanggal">Tanggal</label>
                    <input type="text" name="tanggal" class="form-control" id="exampleInputTanggal" placeholder="Masukkan Tanggal Penyiraman">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputJam">Jam Mulai</label>
                    <input type="text" name="jam_mulai" class="form-control" id="exampleInputJam" placeholder="Masukkan Jam Mulai">
                  </div>
                <div class="form-group">
                    <label for="exampleInputEst1">Estimasi Ambil Air</label>
                    <input type="text" name="estimasi_air" class="form-control" id="exampleInputEst1" placeholder="Masukkan Estimasi Ambil Air">
                  </div>
              <div class="form-group">
                    <label for="exampleInputEst2">Estimasi Selesai</label>
                    <input type="text" name="estimasi_selesai" class="form-control" id="exampleInputEst2" placeholder="Masukkan Estimasi Selesai">
                  </div>
              <!--<div class="form-group">
                    <label for="exampleInputRute">Rute</label>
                    <input type="text" name="rute" class="form-control" id="exampleInputRute" placeholder="Masukkan Rute">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPlat">Plat Kendaraan</label>
                    <input type="text" name="Plat Kendaraan" class="form-control" id="exampleInputPlat" placeholder="Masukkan Plat Kendaraan">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputSopir">Nama Sopir</label>
                    <input type="text" name="Nama Sopir" class="form-control" id="exampleInputSopir" placeholder="Masukkan Nama Sopir">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputRayon">Rayon</label>
                    <input type="text" name="Rayon" class="form-control" id="exampleInputRayon" placeholder="Masukkan Rayon">
                  </div>
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div> -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection