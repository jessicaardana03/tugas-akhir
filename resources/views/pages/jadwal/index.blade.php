@extends('welcome')
@section('content')
    <!-- Content Header (Page header) -->

        <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Jadwal Penyiraman</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <a href="{{ route('jadwal.create')}}">
        <button class="mb-4 btn btn-primary">Tambah Data
        </button>
      </a>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               <!-- <h3 class="card-title">DataTable with minimal features & hover style</h3> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Hari</th>
                    <th>Tanggal</th>
                    <th>Jam Mulai</th>
                    <th>Est. Ambil Air</th>
                    <th>Est. Selesai</th>
                    <!--<th>Rute</th>
                    <th>Plat</th>
                    <th>Nama Sopir</th>
                    <th>Rayon</th> -->
                    <th>Aksi</th> 
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($data_jadwals as $item)
              <tr>
               <td>
               {{$loop->iteration}}
               </td>
               <td>
               {{$item->hari }}
              </td>
              <td>
              {{$item->tanggal }}
              </td>
               <td>
              {{$item->jam_mulai }}
               </td>
               <td>
              {{$item->estimasi_air }}
               </td>
               <td>
              {{$item->estimasi_selesai }}
               </td>
               <td>
        <a class="btn btn-primary btn-sm" href="{{ route('jadwal.edit',$item->id_jadwal) }}" >Edit</a>
        <a href="{{ route('jadwal.delete',$item->id_jadwal) }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
        </td>
      </tr>
               @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
@endsection