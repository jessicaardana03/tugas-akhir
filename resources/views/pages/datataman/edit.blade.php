@extends('welcome')
@section('content')
  <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Update Data Taman</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" action="{{route('datataman.update', ['id'=>$datataman->id_taman])}}" method="post">
                @csrf
                @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputTaman">Nama Taman / Jalan</label>
                    <input type="text" name="nama_taman" class="form-control" id="exampleInputTaman" placeholder="Masukkan Nama Taman / Jalan" value="{{$datataman->nama_taman}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUkuran">Ukuran</label>
                    <input type="text" name="ukuran" class="form-control" id="exampleInputUkuran" placeholder="Masukkan Ukuran" value="{{$datataman->ukuran}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputLokasi">Lokasi</label>
                    <input type="text" name="lokasi" class="form-control" id="exampleInputLokasi" placeholder="Masukkan Lokasi" value="{{$datataman->lokasi}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDepotAir">Depot air</label>
                    <input type="text" name="depot_air" class="form-control" id="exampleInputDepotAir" placeholder="Masukkan Lokasi Depot Air" value="{{$datataman->depot_air}}">
                    <div class="form-group">
                    <label for="exampleInputRute">Rute</label>
                    <input type="text" name="rute" class="form-control" id="exampleInputRute" placeholder="Masukkan Rute Berapa" value="{{$datataman->rute}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputRayon">Rayon</label>
                    <input type="text" name="rayon" class="form-control" id="exampleInputRayon" placeholder="Masukkan Rayon"value="{{$datataman->rayon}}">
                  </div>
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection