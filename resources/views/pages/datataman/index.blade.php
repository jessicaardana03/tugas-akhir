@extends('welcome')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Data Taman</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <a href="{{ route('datataman.create')}}">
          <button class="mb-4 btn btn-primary">Tambah Data
        </button>
          </a>
 
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               <!-- <h3 class="card-title">DataTable with minimal features & hover style</h3> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Taman / Jalan</th>
                    <th>Ukuran</th>
                    <th>Lokasi</th>
                    <th>Depot Air</th>
                    <th>Rute</th>
                    <th>Rayon</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
        @foreach($data_taman as $item)
        <tr>
          <td>
        {{$loop->iteration}}
        </td>
          <td>
          {{$item->nama_taman }}
        </td>
        <td>
          {{$item->ukuran }}
        </td>
          <td>
          {{$item->lokasi }}
        </td>
        <td>
          {{$item->depot_air }}
        </td>
        <td>
          {{$item->rute }}
        </td>
        <td>
          {{$item->rayon }}
        </td>
        <td>
        <a class="btn btn-primary btn-sm" href="{{ route('datataman.edit',$item->id_taman) }}" >Edit</a>
        <a href="{{ route('datataman.delete',$item->id_taman) }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
        </td>
      </tr>
      @endforeach
      </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
@endsection