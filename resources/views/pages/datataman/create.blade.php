@extends('welcome')
@section('content')
  <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Tambah Data Taman</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambahkan Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  action="{{route ('datataman.store')}}" method="POST"> 
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputTaman">Nama Taman</label>
                    <input type="text" name="nama_taman" class="form-control" id="exampleInputTaman" placeholder="Masukkan Nama Taman">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUkuran">Ukuran</label>
                    <input type="text" name="ukuran" class="form-control" id="exampleInputUkuran" placeholder="Masukkan Ukuran">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputLokasi">Lokasi</label>
                    <input type="text" name="lokasi" class="form-control" id="exampleInputLokasi" placeholder="Masukkan Lokasi">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDepotAir">Depot air</label>
                    <input type="text" name="depot_air" class="form-control" id="exampleInputDepotAir" placeholder="Masukkan Lokasi Depot Air">
                    <div class="form-group">
                    <label for="exampleInputRute">Rute</label>
                    <input type="text" name="rute" class="form-control" id="exampleInputRute" placeholder="Masukkan Rute Berapa">
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputRayon">Rayon</label>
                    <input type="text" name="rayon" class="form-control" id="exampleInputRayon" placeholder="Masukkan Rayon">
                  </div>
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection