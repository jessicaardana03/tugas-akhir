@extends('welcome')
@section('content')
    <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Data Sopir</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <a href="{{ route('datasopir.create')}}">
        <button class="mb-4 btn btn-primary">Tambah Data
        </button>
      </a>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               <!-- <h3 class="card-title">DataTable with minimal features & hover style</h3> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Sopir</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Rayon</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody> 
                   @foreach($datasopir as $data)
                    <tr>
                      <td>
                        {{$loop->iteration}}
                      </td>
                      <td>
                        {{$data->nama_sopir}}
                      </td>
                      <td>
                        {{$data->username}}
                      </td>
                      <td>
                        {{$data->passsword}}
                      </td>
                      <td>
                        {{$data->rayon}}
                      </td>
                      <td>
        <a class="btn btn-primary btn-sm" href="{{ route('datasopir.edit',$data->id_sopir) }}" >Edit</a>
        <a href="{{ route('datasopir.delete',$data->id_sopir) }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
        </td>
                    </tr>
                    @endforeach
                  </tbody>    
                </table>
              </div>
              <!-- /.card-body -->
            </div>      
@endsection