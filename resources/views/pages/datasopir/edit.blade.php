@extends('welcome')
@section('content')
 <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Update Data Sopir</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" action="{{route('datasopir.update', ['id'=>$datasopir->id_sopir])}}" method="post">
                @csrf
                @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputSopir">Nama Sopir</label>
                    <input type="text" name="nama_sopir" class="form-control" id="exampleInputSopir" placeholder="Masukkan Nama Sopir" value="{{$datasopir->nama_sopir}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUsername">Username</label>
                    <input type="text" name="username" class="form-control" id="exampleInputUsername
                    " placeholder="Masukkan Username" value="{{$datasopir->username}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword">Password</label>
                    <input type="text" name="password" class="form-control" id="exampleInputPassword" placeholder="Masukkan Password" value="{{$datasopir->password}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInpurRayon">Rayon</label>
                    <input type="text" name="rayon" class="form-control" id="exampleInputRayon" placeholder="Masukkan Password" value="{{$datasopir->rayon}}">
                  </div>
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection