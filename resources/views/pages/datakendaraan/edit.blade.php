@section('content')
@extends('welcome')
 <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <h1 class="text-center">Update Data Kendaraan</h1>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" action="{{route('datakendaraan.update', ['id'=>$datakendaraan->id_kendaraan])}}" method="post">
                @csrf
                @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputSopir">Nama Sopir</label>
                    <input type="text" name="nama_sopir" class="form-control" id="exampleInputSopir" placeholder="Masukkan Nama Sopir" value="{{$datakendaraan->nama_sopir}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputTangki">Kapasitas Kendaraan</label>
                    <input type="text" name="kapasitas_kendaraan" class="form-control" id="exampleInputTangki" placeholder="Masukkan Kapasitas Kendaraan" value="{{$datakendaraan->kapasitas_kendaraan}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputAir">Kapasitas Air</label>
                    <input type="text" name="kapasitas_air" class="form-control" id="exampleInputAir" placeholder="Masukkan Kapasitas Air" value="{{$datakendaraan->kapasitas_air}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPlat">Plat Kendaraan</label>
                    <input type="text" name="plat_kendaraan" class="form-control" id="exampleInputPlat" placeholder="Masukkan Plat Kendaraan" value="{{$datakendaraan->plat_kendaraan}}">
                  </div>
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection