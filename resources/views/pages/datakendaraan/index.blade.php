@extends('welcome')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <h1 class="text-center">Data Kendaraan</h1>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <a href="{{ route('datakendaraan.create')}}">
      <button class="mb-4 btn btn-primary">Tambah Data
      </button>
    </a>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <!-- <h3 class="card-title">DataTable with minimal features & hover style</h3> -->
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Sopir</th>
                  <th>Kapasitas Kendaraan</th>
                  <th>Kapasitas Air</th>
                  <th>Plat Kendaraan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @php
                $angka=1
                @endphp
                @foreach($datakendaraan as $data)
                <tr>
                  <td>
                    {{$loop->iteration}}
                  </td>
                  <td>
                    {{$data->nama_sopir}}
                  </td>
                  <td>
                    {{$data->kapasitas_kendaraan}}
                  </td>
                  <td>
                    {{$data->kapasitas_air}}
                  </td>
                  <td>
                    {{$data->plat_kendaraan}}
                  </td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{ route('datakendaraan.edit',$data->id_kendaraan) }}">Edit</a>
                    <a href="{{ route('datakendaraan.delete',$data->id_kendaraan) }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
        @endsection