   <a class="brand-link">
      <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Operator</span>
    </a>
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/avatar2.png" class="img-circle elevation-2" alt="User Image">
        </div>
          <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-top" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                          </ul>

        
      </div>

          <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('/datakendaraan') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Data Kendaraan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <!--<ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../layout/top-nav.html" class="nav-link">
                </a>
              </li>
            </ul> -->
          </li>
           <li class="nav-item">
            <a href="{{ url('/datajadwal') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Jadwal
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <!--<ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../layout/top-nav.html" class="nav-link">
                </a>
              </li>
            </ul> -->
          </li>
            <li class="nav-item">
            <a href="{{ url('/datasopir') }}" class="nav-link">
              <i class="nav-icon fas fas fa-edit"></i>
              <p>
                Data Sopir
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <!--<ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../layout/top-nav.html" class="nav-link">
                </a>
              </li>
            </ul> -->
          </li>
          <li class="nav-item">
            <a href="{{ url('/datataman') }}" class="nav-link">
              <i class="nav-icon fas fas fa-table"></i>
              <p>
                Data Taman
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <!--<ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../layout/top-nav.html" class="nav-link">
                </a>
              </li>
            </ul> -->

      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->